﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MyNetworkManager : NetworkManager
{ 
    public void MyStartHost()
    {
        Debug.Log(Time.realtimeSinceStartup + " starting host");
        StartHost();
    }

    public override void OnStartHost()
    {
        Debug.Log(Time.realtimeSinceStartup + " host started " );
    }

    public override void OnStartClient(NetworkClient myClient)
    {
        Debug.Log(Time.realtimeSinceStartup + " client requesting " );
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        Debug.Log(Time.realtimeSinceStartup + " client connect " + conn.address );
    }
}
